// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   DateFormatter.java

package com.ssi.util;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.Vector;

public class DateFormatter
{

    public DateFormatter()
    {
    }

    public String convertMonthToMMM(GregorianCalendar fecha)
    {
        int mes = fecha.get(2);
        String mesTraducido = "";
        switch(mes)
        {
        case 0: // '\0'
            mesTraducido = "Ene";
            break;

        case 1: // '\001'
            mesTraducido = "Feb";
            break;

        case 2: // '\002'
            mesTraducido = "Mar";
            break;

        case 3: // '\003'
            mesTraducido = "Abr";
            break;

        case 4: // '\004'
            mesTraducido = "May";
            break;

        case 5: // '\005'
            mesTraducido = "Jun";
            break;

        case 6: // '\006'
            mesTraducido = "Jul";
            break;

        case 7: // '\007'
            mesTraducido = "Ago";
            break;

        case 8: // '\b'
            mesTraducido = "Sep";
            break;

        case 9: // '\t'
            mesTraducido = "Oct";
            break;

        case 10: // '\n'
            mesTraducido = "Nov";
            break;

        case 11: // '\013'
            mesTraducido = "Dic";
            break;
        }
        return mesTraducido;
    }

    public String convertToAAAAMMDD(GregorianCalendar fecha)
    {
        String mes = Integer.toString(fecha.get(2) + 1);
        String dia = Integer.toString(fecha.get(5));
        if(mes.length() == 1)
            mes = "0" + mes;
        if(dia.length() == 1)
            dia = "0" + dia;
        String fechaString = fecha.get(1) + "-" + mes + "-" + dia;
        return fechaString;
    }

    public GregorianCalendar convertToCalendar(String stringFecha)
    {
        String fecha = stringFecha;
        int dia = Integer.parseInt(fecha.substring(0, fecha.indexOf("-")));
        int anio = Integer.parseInt(fecha.substring(fecha.lastIndexOf("-") + 1, fecha.lastIndexOf("-") + 5));
        String fechaAux1 = fecha.substring(fecha.indexOf("-") + 1);
        String fechaAux2 = fechaAux1.substring(0, fechaAux1.indexOf("-"));
        int mes = 0;
        if("Ene".equals(fechaAux2))
            mes = 0;
        if("Feb".equals(fechaAux2))
            mes = 1;
        if("Mar".equals(fechaAux2))
            mes = 2;
        if("Abr".equals(fechaAux2))
            mes = 3;
        if("May".equals(fechaAux2))
            mes = 4;
        if("Jun".equals(fechaAux2))
            mes = 5;
        if("Jul".equals(fechaAux2))
            mes = 6;
        if("Ago".equals(fechaAux2))
            mes = 7;
        if("Sep".equals(fechaAux2))
            mes = 8;
        if("Oct".equals(fechaAux2))
            mes = 9;
        if("Nov".equals(fechaAux2))
            mes = 10;
        if("Dic".equals(fechaAux2))
            mes = 11;
        return new GregorianCalendar(anio, mes, dia);
    }

    public GregorianCalendar convertToCalendar(Date aDate)
    {
        GregorianCalendar answer;
        if(aDate != null)
        {
            answer = new GregorianCalendar(aDate.getDate(), aDate.getMonth(), aDate.getYear() + 1900);
            answer.setTime(aDate);
        } else
        {
            answer = null;
        }
        return answer;
    }

    public String convertToDDMMAAAA(GregorianCalendar fecha)
    {
        if(fecha == null)
            return new String();
        String mes = Integer.toString(fecha.get(2) + 1);
        String dia = Integer.toString(fecha.get(5));
        if(mes.length() == 1)
            mes = "0" + mes;
        if(dia.length() == 1)
            dia = "0" + dia;
        String fechaString = dia + "/" + mes + "/" + fecha.get(1);
        return fechaString;
    }

    public String convertToDDMMMAAAA(GregorianCalendar fecha)
    {
        String mesTraducido = convertMonthToMMM(fecha);
        String fechaString = fecha.get(5) + "-" + mesTraducido + "-" + fecha.get(1);
        return fechaString;
    }

    public Vector get24Meses()
    {
        Vector misMeses = new Vector();
        GregorianCalendar fecha = new GregorianCalendar();
        fecha.set(5, 1);
        int esteAno = fecha.get(1) - 1;
        int esteMes = fecha.get(2);
        fecha.set(1, esteAno);
        for(int i = 0; i < 24; i++)
        {
            String mesString = getSpanishMonth(fecha) + " " + fecha.get(1);
            misMeses.addElement(mesString);
            if(fecha.get(2) == 11)
            {
                esteAno++;
                fecha.set(1, esteAno);
                esteMes = 0;
                fecha.set(2, esteMes);
            } else
            {
                esteMes++;
                fecha.set(2, esteMes);
            }
        }

        return misMeses;
    }

    public GregorianCalendar getDateFromDDMMYYYY(String aDateString, String separator)
    {
        StringTokenizer miTok = new StringTokenizer(aDateString, separator);
        int dia = (new Integer(miTok.nextToken())).intValue();
        int mes = (new Integer(miTok.nextToken())).intValue();
        int ano = (new Integer(miTok.nextToken())).intValue();
        return new GregorianCalendar(ano, mes - 1, dia);
    }

    public GregorianCalendar getDateFromSpanishMonthString(String mesString)
    {
        StringTokenizer miTok = new StringTokenizer(mesString);
        int mes = -1;
        String stringMes = miTok.nextToken();
        int ano = Integer.valueOf(miTok.nextToken()).intValue();
        if(stringMes.equals("Enero"))
            mes = 0;
        else
        if(stringMes.equals("Febrero"))
            mes = 1;
        else
        if(stringMes.equals("Marzo"))
            mes = 2;
        else
        if(stringMes.equals("Abril"))
            mes = 3;
        else
        if(stringMes.equals("Mayo"))
            mes = 4;
        else
        if(stringMes.equals("Junio"))
            mes = 5;
        else
        if(stringMes.equals("Julio"))
            mes = 6;
        else
        if(stringMes.equals("Agosto"))
            mes = 7;
        else
        if(stringMes.equals("Septiembre"))
            mes = 8;
        else
        if(stringMes.equals("Octubre"))
            mes = 9;
        else
        if(stringMes.equals("Noviembre"))
            mes = 10;
        else
        if(stringMes.equals("Diciembre"))
            mes = 11;
        GregorianCalendar fecha = new GregorianCalendar(ano, mes, 1);
        return fecha;
    }

    public GregorianCalendar getDateFromString(String aDateString)
    {
        return getDateFromDDMMYYYY(aDateString, "/");
    }

    public GregorianCalendar getDateFromYYYYMMDD(String aDateString, String separator)
    {
        StringTokenizer miTok = new StringTokenizer(aDateString, separator);
        int ano = (new Integer(miTok.nextToken())).intValue();
        int mes = (new Integer(miTok.nextToken())).intValue();
        int dia = (new Integer(miTok.nextToken())).intValue();
        return new GregorianCalendar(ano, mes - 1, dia);
    }

    public int getNumeroDias(GregorianCalendar miFecha)
    {
        int dias = 0;
        int esteMes = miFecha.get(2);
        GregorianCalendar tempFecha = new GregorianCalendar(miFecha.get(1), miFecha.get(2), miFecha.get(5));
        tempFecha.set(5, 31);
        if(esteMes == tempFecha.get(2))
        {
            dias = 31;
        } else
        {
            tempFecha = (GregorianCalendar)miFecha.clone();
            tempFecha.set(5, 30);
            if(esteMes == tempFecha.get(2))
            {
                dias = 30;
            } else
            {
                tempFecha = (GregorianCalendar)miFecha.clone();
                tempFecha.set(5, 29);
                if(esteMes == tempFecha.get(2))
                {
                    dias = 29;
                } else
                {
                    tempFecha = (GregorianCalendar)miFecha.clone();
                    tempFecha.set(5, 28);
                    if(esteMes == tempFecha.get(2))
                        dias = 28;
                }
            }
        }
        return dias;
    }

    public GregorianCalendar getPrimeroDeMes(GregorianCalendar unaFecha)
    {
        int primero = 1;
        int dia = unaFecha.get(5);
        int mes = unaFecha.get(2);
        int anio = unaFecha.get(1);
        if(dia == primero)
        {
            return unaFecha;
        } else
        {
            GregorianCalendar fecha = new GregorianCalendar(anio, mes, primero);
            return fecha;
        }
    }

    public String getSpanishDay(GregorianCalendar fecha)
    {
        int dia = fecha.get(7);
        String diaTraducido = "";
        switch(dia)
        {
        case 1: // '\001'
            diaTraducido = "Domingo";
            break;

        case 2: // '\002'
            diaTraducido = "Lunes";
            break;

        case 3: // '\003'
            diaTraducido = "Martes";
            break;

        case 4: // '\004'
            diaTraducido = "Mi\uFFE9rcoles";
            break;

        case 5: // '\005'
            diaTraducido = "Jueves";
            break;

        case 6: // '\006'
            diaTraducido = "Viernes";
            break;

        case 7: // '\007'
            diaTraducido = "S\uFFE1bado";
            break;
        }
        return diaTraducido;
    }

    public String getSpanishMonth(GregorianCalendar estaFecha)
    {
        String stringMes = null;
        int mes = estaFecha.get(2);
        switch(mes)
        {
        case 0: // '\0'
            stringMes = "Enero";
            break;

        case 1: // '\001'
            stringMes = "Febrero";
            break;

        case 2: // '\002'
            stringMes = "Marzo";
            break;

        case 3: // '\003'
            stringMes = "Abril";
            break;

        case 4: // '\004'
            stringMes = "Mayo";
            break;

        case 5: // '\005'
            stringMes = "Junio";
            break;

        case 6: // '\006'
            stringMes = "Julio";
            break;

        case 7: // '\007'
            stringMes = "Agosto";
            break;

        case 8: // '\b'
            stringMes = "Septiembre";
            break;

        case 9: // '\t'
            stringMes = "Octubre";
            break;

        case 10: // '\n'
            stringMes = "Noviembre";
            break;

        case 11: // '\013'
            stringMes = "Diciembre";
            break;
        }
        return stringMes;
    }

    public String getStringFechaCompleta(GregorianCalendar estaFecha)
    {
        String fecha = getSpanishDay(estaFecha) + ", ";
        fecha = fecha + String.valueOf(estaFecha.get(5)) + " de ";
        fecha = fecha + getSpanishMonth(estaFecha) + " de ";
        fecha = fecha + String.valueOf(estaFecha.get(1));
        return fecha;
    }

    public String getTimestampStringFrom(GregorianCalendar fecha)
    {
        return convertToAAAAMMDD(fecha) + " " + getTimeStringFrom(fecha);
    }

    public String getTimeStringFrom(GregorianCalendar fecha)
    {
        String answer = new String();
        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setParseIntegerOnly(true);
        formatter.setMaximumIntegerDigits(2);
        formatter.setMinimumIntegerDigits(2);
        String hours = formatter.format(fecha.get(11));
        answer = answer + hours;
        String minutes = formatter.format(fecha.get(12));
        answer = answer + ":" + minutes;
        String seconds = formatter.format(fecha.get(13));
        answer = answer + ":" + seconds;
        return answer;
    }

    public String InvertDate(GregorianCalendar fecha)
    {
        String InvertedDate = "";
        if(fecha.get(5) < 10)
            InvertedDate = "0" + fecha.get(5);
        else
            InvertedDate = String.valueOf(fecha.get(5));
        if(fecha.get(2) < 9)
            InvertedDate = InvertedDate + "0" + (fecha.get(2) + 1) + fecha.get(1);
        else
            InvertedDate = InvertedDate + (fecha.get(2) + 1) + fecha.get(1);
        return InvertedDate;
    }

    public boolean isWeekend(GregorianCalendar estaFecha)
    {
        int DayOfWeek = 0;
        boolean isWknd = true;
        DayOfWeek = estaFecha.get(7);
        if(DayOfWeek == 7 || DayOfWeek == 1)
            isWknd = true;
        else
            isWknd = false;
        return isWknd;
    }

    public GregorianCalendar setearHoraA24(GregorianCalendar original)
    {
        GregorianCalendar sinHora = (GregorianCalendar)original.clone();
        sinHora.set(11, 23);
        sinHora.set(12, 59);
        sinHora.set(13, 59);
        sinHora.set(14, 59);
        return sinHora;
    }

    public GregorianCalendar setearHoraACero(GregorianCalendar original)
    {
        GregorianCalendar sinHora = (GregorianCalendar)original.clone();
        sinHora.set(11, 0);
        sinHora.set(12, 0);
        sinHora.set(13, 0);
        sinHora.set(14, 0);
        return sinHora;
    }
}
