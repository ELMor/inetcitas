// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   SmtpClient.java

package com.ssi.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Date;
import java.util.StringTokenizer;

public class SmtpClient
{

    private static final int SMTP_ERROR_CODE1 = 52;
    private static final int SMTP_ERROR_CODE2 = 53;
    public static final boolean DEBUG = false;

    private static void readReply(BufferedReader reader)
        throws IOException
    {
        String reply = reader.readLine();
        char statusCode = reply.charAt(0);
        if((statusCode == '4') | (statusCode == '5'))
            throw new IOException("SMTP: " + reply);
        else
            return;
    }

    private static void sendCommand(DataOutputStream out, String command)
        throws IOException
    {
        out.writeBytes(command + "\r\n");
    }

    public static void sendMail(String mailServerHost, int port, String domain, String sender, String recipients, String subject, String body)
        throws IOException
    {
        try
        {
            Socket mailSocket = new Socket(mailServerHost, port);
            BufferedReader socketIn = new BufferedReader(new InputStreamReader(mailSocket.getInputStream()));
            DataOutputStream socketOut = new DataOutputStream(mailSocket.getOutputStream());
            readReply(socketIn);
            sendCommand(socketOut, "HELO " + domain);
            readReply(socketIn);
            sendCommand(socketOut, "MAIL FROM: " + sender);
            readReply(socketIn);
            for(StringTokenizer tokenizer = new StringTokenizer(recipients, ","); tokenizer.hasMoreTokens(); readReply(socketIn))
                sendCommand(socketOut, "RCPT TO: " + tokenizer.nextToken());

            String mailMessage = "Date: " + (new Date()).toString() + "\r\n" + "From: " + sender + "\r\n" + "To: " + recipients + "\r\n" + "Subject: " + subject + "\r\n" + "\r\n" + body + "\r\n";
            sendCommand(socketOut, "DATA");
            readReply(socketIn);
            sendCommand(socketOut, mailMessage + ".");
            readReply(socketIn);
            sendCommand(socketOut, "QUIT");
            readReply(socketIn);
        }
        catch(IOException ioe)
        {
            System.out.println(" !!! ERROR -> SmtpClient::sendMail -> IOException at " + (new Date()).toString());
            System.out.println(" " + ioe);
            throw new IOException("SmtpClient: ");
        }
    }

    public SmtpClient()
    {
    }
}
