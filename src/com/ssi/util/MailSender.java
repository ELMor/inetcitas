// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   MailSender.java

package com.ssi.util;


// Referenced classes of package com.ssi.util:
//            MailThread

public class MailSender
{

    protected MailThread mailThread;
    protected String mailServerHost;
    protected int portNumber;
    protected String domainName;
    protected String sender;
    protected String recipient;
    protected String subject;
    protected String body;

    public MailSender(String aMailServerHost, int aPortNumber, String aDomainName, String aSender, String aRecipient, String aSubject, String aBody)
    {
        mailServerHost = aMailServerHost;
        portNumber = aPortNumber;
        domainName = aDomainName;
        sender = aSender;
        recipient = aRecipient;
        subject = aSubject;
        body = aBody;
    }

    public String getBody()
    {
        return body;
    }

    public String getDomainName()
    {
        return domainName;
    }

    public String getMailServerHost()
    {
        return mailServerHost;
    }

    protected MailThread getMailThread()
    {
        return mailThread;
    }

    public int getPortNumber()
    {
        return portNumber;
    }

    public String getRecipient()
    {
        return recipient;
    }

    public String getSender()
    {
        return sender;
    }

    public String getSubject()
    {
        return subject;
    }

    public void sendMail()
    {
        setMailThread(new MailThread(getMailServerHost(), getPortNumber(), getDomainName(), getSender(), getRecipient(), getSubject(), getBody()));
        getMailThread().start();
    }

    public void setBody(String newBody)
    {
        body = newBody;
    }

    public void setDomainName(String newDomainName)
    {
        domainName = newDomainName;
    }

    public void setMailServerHost(String newMailServerHost)
    {
        mailServerHost = newMailServerHost;
    }

    protected void setMailThread(MailThread newMailThread)
    {
        mailThread = newMailThread;
    }

    public void setPortNumber(int newPortNumber)
    {
        portNumber = newPortNumber;
    }

    public void setRecipient(String newRecipient)
    {
        recipient = newRecipient;
    }

    public void setSender(String newSender)
    {
        sender = newSender;
    }

    public void setSubject(String newSubject)
    {
        subject = newSubject;
    }
}
