// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   Utils.java

package com.ssi.util;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.text.Format;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUtils;

// Referenced classes of package com.ssi.util:
//            VStat, MailThread

public class Utils
{

    public static void enviarMail(String pmMensaje)
    {
        String host = VStat.host;
        int port = VStat.port;
        String domain = VStat.domain;
        String sender = VStat.sender;
        String recipients = VStat.recipients;
        String subject = "Error en la aplicaci\363n Consulta de Citas";
        String body = "Ha ocurrido el siguiente error tratando de ejecutar una sentencia SQL : " + pmMensaje;
        MailThread mailer = new MailThread(host, port, domain, sender, recipients, subject, body);
        mailer.start();
    }

    public static String disallowEmptyHtmlString(String theString)
    {
        String myNewString = "";
        if(theString == null)
            myNewString = "&nbsp;";
        else
        if(theString.length() == 0)
            myNewString = "&nbsp;";
        else
            myNewString = theString;
        return myNewString;
    }

    public static String makeStringCorrectLength(String theString, int theLength, String paddingCharacter, boolean padBefore)
    {
        if(theString.length() > theLength)
            theString = theString.substring(0, theLength);
        else
            theString = padString(theString, theLength, paddingCharacter, padBefore);
        return theString;
    }

    public static String obtenerNombreMes(String pmMes)
    {
        String nombreMes = "Desconocido";
        int mes = 0;
        try
        {
            mes = Integer.parseInt(pmMes);
            switch(mes)
            {
            case 1: // '\001'
                nombreMes = "Enero";
                break;

            case 2: // '\002'
                nombreMes = "Febrero";
                break;

            case 3: // '\003'
                nombreMes = "Marzo";
                break;

            case 4: // '\004'
                nombreMes = "Abril";
                break;

            case 5: // '\005'
                nombreMes = "Mayo";
                break;

            case 6: // '\006'
                nombreMes = "Junio";
                break;

            case 7: // '\007'
                nombreMes = "Julio";
                break;

            case 8: // '\b'
                nombreMes = "Agosto";
                break;

            case 9: // '\t'
                nombreMes = "Setiembre";
                break;

            case 10: // '\n'
                nombreMes = "Octubre";
                break;

            case 11: // '\013'
                nombreMes = "Noviembre";
                break;

            case 12: // '\f'
                nombreMes = "Diciembre";
                break;
            }
        }
        catch(NumberFormatException _ex) { }
        return nombreMes;
    }

    public static String padString(String myString, int finalLength, String padWithThisCharacter, boolean padBefore)
    {
        int orginalLength = myString.length();
        for(int currentLength = orginalLength; currentLength < finalLength; currentLength++)
            if(padBefore)
                myString = padWithThisCharacter + myString;
            else
                myString = myString + padWithThisCharacter;

        return myString;
    }

    public static String fechaMasSieteDias()
    {
        String fechaMasSieteDias = "";
        String diaaux = "";
        String mesaux = "";
        String anioaux = "";
        Date aDate = new Date();
        GregorianCalendar calendar = new GregorianCalendar(aDate.getDate(), aDate.getMonth(), aDate.getYear() + 1900);
        calendar.setTime(aDate);
        calendar.add(5, 7);
        int dia = calendar.get(5);
        int mes = calendar.get(2) + 1;
        int anio = calendar.get(1);
        if(dia < 10)
            diaaux = "0" + dia;
        else
            diaaux = String.valueOf(dia);
        if(mes < 10)
            mesaux = "0" + mes;
        else
            mesaux = String.valueOf(mes);
        fechaMasSieteDias = diaaux + "/" + mesaux + "/" + anio;
        return fechaMasSieteDias;
    }

    public static String fechaMasXDias(String fechadesde, String pdias)
    {
        String fechaMasXDias = "";
        int xdias = Integer.parseInt(pdias);
        String diaaux = "";
        String mesaux = "";
        String anioaux = "";
        Date aDate = convertirFechaADate(fechadesde);
        GregorianCalendar calendar = new GregorianCalendar(aDate.getDate(), aDate.getMonth(), aDate.getYear() + 1900);
        calendar.setTime(aDate);
        calendar.add(5, xdias);
        int dia = calendar.get(5);
        int mes = calendar.get(2) + 1;
        int anio = calendar.get(1);
        if(dia < 10)
            diaaux = "0" + dia;
        else
            diaaux = String.valueOf(dia);
        if(mes < 10)
            mesaux = "0" + mes;
        else
            mesaux = String.valueOf(mes);
        fechaMasXDias = diaaux + "/" + mesaux + "/" + anio;
        return fechaMasXDias;
    }

    public static String padString(String myString, int finalLength, String padWithTheCharacter)
    {
        return padString(myString, finalLength, padWithTheCharacter, false);
    }

    public static String padStringBefore(String myString, int finalLength, String padWithTheCharacter)
    {
        return padString(myString, finalLength, padWithTheCharacter, true);
    }

    public static String padStringHTML(String myString, int finalLength)
    {
        return padString(myString, finalLength, "&nbsp;");
    }

    public static String createHiddenFieldHTML(String name, String value)
    {
        return "<INPUT type=hidden name=\"" + name + "\" value=\"" + value + "\">\n";
    }

    public static String formatNumber(String text, String seperatorBetweenWholeAndFraction, String seperatorBetweenThousands)
    {
        String resultado = "";
        String parteDecimalS = "00";
        text = text.trim();
        if(numeroValido(text, seperatorBetweenWholeAndFraction))
        {
            StringTokenizer st = new StringTokenizer(text, seperatorBetweenWholeAndFraction);
            String parteEnteraS = st.nextToken();
            parteEnteraS = eliminoCerosIzquierda(parteEnteraS);
            if(st.hasMoreTokens())
                parteDecimalS = st.nextToken();
            int largoEntero = parteEnteraS.length();
            if(largoEntero <= 3)
            {
                resultado = parteEnteraS;
                if(!parteDecimalS.equals(""))
                    resultado = resultado + seperatorBetweenWholeAndFraction + parteDecimalS;
            } else
            {
                int cantidadPuntos = largoEntero / 3;
                int posicionPrimerPunto = largoEntero - cantidadPuntos * 3;
                int posicionInicial = 0;
                int posicionFinal = 0;
                if(posicionPrimerPunto == 0)
                {
                    posicionFinal = 3;
                    cantidadPuntos--;
                } else
                {
                    posicionFinal = posicionPrimerPunto--;
                }
                resultado = text.substring(posicionInicial, posicionFinal);
                for(int i = 0; i < cantidadPuntos; i++)
                {
                    posicionInicial = posicionFinal;
                    posicionFinal += 3;
                    resultado = resultado + seperatorBetweenThousands + text.substring(posicionInicial, posicionFinal);
                }

                if(!parteDecimalS.equals(""))
                    resultado = resultado + seperatorBetweenWholeAndFraction + parteDecimalS;
            }
        } else
        {
            resultado = text;
        }
        return resultado;
    }

    public static String formatNumber0(String text, String seperatorBetweenWholeAndFraction, String seperatorBetweenThousands)
    {
        String resultado = formatNumber(text, seperatorBetweenWholeAndFraction, seperatorBetweenThousands);
        if(resultado.equals(""))
            resultado = "0";
        return resultado;
    }

    public static String separateDecimals(String text, String seperatorBetweenWholeAndFraction, int quantityDecimals)
    {
        int largoString = text.length();
        String resultado;
        if(largoString > quantityDecimals)
        {
            int finParteEntera = largoString - quantityDecimals;
            String entero = text.substring(0, finParteEntera);
            String decimales = text.substring(finParteEntera);
            resultado = entero + seperatorBetweenWholeAndFraction + decimales;
        } else
        {
            resultado = text;
        }
        return resultado;
    }

    public static String formatFloat(float theNumber_float, int numberOfDecimalPlaces, String seperatorBetweenWholeAndFraction, String seperatorBetweenThousands)
    {
        String result = "";
        Float theNumber_Float = new Float(theNumber_float);
        int wholeNumberPart = theNumber_Float.intValue();
        String wholeNumberPart_String = Integer.toString(wholeNumberPart);
        float fractionalPart_float = theNumber_float - (float)wholeNumberPart;
        String fractionalPart_String = Float.toString(fractionalPart_float);
        fractionalPart_String = fractionalPart_String.substring(2, fractionalPart_String.length());
        result = result + formatNumber(wholeNumberPart_String, seperatorBetweenWholeAndFraction, seperatorBetweenThousands);
        result = result + seperatorBetweenWholeAndFraction;
        if(numberOfDecimalPlaces <= 0)
        {
            result = result + "0";
        } else
        {
            int lengthOfFractionalPart = fractionalPart_String.length();
            for(int i = 0; i < numberOfDecimalPlaces; i++)
            {
                if(i >= lengthOfFractionalPart)
                    break;
                result = result + fractionalPart_String.substring(i, i + 1);
            }

        }
        return result;
    }

    public static boolean numeroValido(String text, String seperatorBetweenWholeAndFraction)
    {
        boolean resultado = true;
        String parteEnteraS = null;
        String parteDecimalS = null;
        StringTokenizer st = new StringTokenizer(text, seperatorBetweenWholeAndFraction);
        parteEnteraS = st.nextToken();
        if(st.hasMoreTokens())
            parteDecimalS = st.nextToken();
        if(st.hasMoreTokens())
            resultado = false;
        else
            try
            {
                int parteEnteraI = Integer.parseInt(parteEnteraS);
                int parteDecimalI;
                if(parteDecimalS != null)
                    parteDecimalI = Integer.parseInt(parteDecimalS);
            }
            catch(Exception _ex)
            {
                resultado = false;
            }
        return resultado;
    }

    public static String eliminoCerosIzquierda(String text)
    {
        if(text.indexOf("0") == 0)
            for(; text.indexOf("0") == 0; text = text.substring(1));
        return text;
    }

    public static boolean isLengthOfStringCorrect(String aString, int desiredLength)
    {
        boolean isLengthCorrect = false;
        if(aString.length() == desiredLength)
            isLengthCorrect = true;
        else
            isLengthCorrect = false;
        return isLengthCorrect;
    }

    public static boolean isInteger(String aString)
    {
        boolean isInteger = true;
        if(aString == null)
        {
            isInteger = false;
        } else
        {
            aString = aString.trim();
            if(aString.equals(""))
                isInteger = false;
            else
            if(aString.equals("-"))
                isInteger = false;
            else
            if(aString.equals("-0"))
            {
                isInteger = false;
            } else
            {
                char myCharArray[] = aString.toCharArray();
                int startIndex;
                if(myCharArray[0] == '-')
                    startIndex = 1;
                else
                    startIndex = 0;
                int lengthOfCharArray = myCharArray.length;
                for(int i = startIndex; i < lengthOfCharArray; i++)
                    if(!Character.isDigit(myCharArray[i]))
                        isInteger = false;

            }
        }
        return isInteger;
    }

    public static String handleSingleQuotes(String rawString)
    {
        StringTokenizer myTokenizer = new StringTokenizer(rawString, "'");
        String processedString = "";
        if(myTokenizer.hasMoreTokens())
            processedString = myTokenizer.nextToken();
        while(myTokenizer.hasMoreTokens()) 
        {
            processedString = processedString.concat("''");
            processedString = processedString.concat(myTokenizer.nextToken());
        }
        return processedString;
    }

    public static String removeSingleQuotes(String rawString)
    {
        StringTokenizer myTokenizer = new StringTokenizer(rawString, "'");
        String processedString = "";
        if(myTokenizer.hasMoreTokens())
            processedString = myTokenizer.nextToken();
        while(myTokenizer.hasMoreTokens()) 
            processedString = processedString.concat(myTokenizer.nextToken());
        return processedString;
    }

    public static String removeDoubleQuotes(String rawString)
    {
        StringTokenizer myTokenizer = new StringTokenizer(rawString, "\"");
        String processedString = "";
        if(myTokenizer.hasMoreTokens())
            processedString = myTokenizer.nextToken();
        while(myTokenizer.hasMoreTokens()) 
            processedString = processedString.concat(myTokenizer.nextToken());
        return processedString;
    }

    public static String leftTrimStringIncludingNBSP(String rawString)
    {
        String processedString = "";
        if(rawString != null)
            processedString = rawString;
        char charArray[] = processedString.toCharArray();
        boolean stopChecking = false;
        int indexOfLastSpace = -1;
        for(int i = 0; i < charArray.length && !stopChecking; i++)
            if(Character.isWhitespace(charArray[i]) || Character.isSpaceChar(charArray[i]))
                indexOfLastSpace = i;
            else
                stopChecking = true;

        return processedString.substring(indexOfLastSpace + 1);
    }

    public static String rightTrimStringIncludingNBSP(String rawString)
    {
        String processedString = "";
        if(rawString != null)
            processedString = rawString;
        char charArray[] = processedString.toCharArray();
        boolean stopChecking = false;
        int firstSpaceCharacter = charArray.length;
        for(int i = charArray.length - 1; (i >= 0) & (!stopChecking); i--)
            if(Character.isWhitespace(charArray[i]) || Character.isSpaceChar(charArray[i]))
                firstSpaceCharacter = i;
            else
                stopChecking = true;

        return processedString.substring(0, firstSpaceCharacter);
    }

    public static String trimStringIncludingNBSP(String rawString)
    {
        String processedString = leftTrimStringIncludingNBSP(rawString);
        processedString = rightTrimStringIncludingNBSP(processedString);
        return processedString;
    }

    public static String takeNewLines(String myString)
    {
        boolean notMore = true;
        String auxiliarString1 = "";
        String auxiliarString2 = "";
        char charArray[] = myString.toCharArray();
        for(int i = 0; i < charArray.length; i++)
            if(Character.isWhitespace(charArray[i]) && !Character.isSpaceChar(charArray[i]))
            {
                auxiliarString1 = auxiliarString1.concat(" ");
            } else
            {
                auxiliarString2 = (new Character(charArray[i])).toString();
                auxiliarString1 = auxiliarString1.concat(auxiliarString2);
            }

        return auxiliarString1;
    }

    public static String makeStringCorrectLength(String theString, int theLength, String paddingCharacter)
    {
        return makeStringCorrectLength(theString, theLength, paddingCharacter, false);
    }

    public static String dumpRequest(HttpServletRequest request)
    {
        String theRequestInfo = "\n";
        String theRequestUrl = HttpUtils.getRequestURL(request).toString();
        theRequestInfo = theRequestInfo + " theRequestUrl = >" + theRequestUrl + "<\n";
        String theRequestMethod = request.getMethod();
        theRequestInfo = theRequestInfo + " Request method = >" + theRequestMethod + "<\n";
        String theRequestUri = request.getRequestURI();
        theRequestInfo = theRequestInfo + " Request URI = >" + theRequestUri + "<\n";
        String theRequestProtocol = request.getProtocol();
        theRequestInfo = theRequestInfo + " Request protocol = >" + theRequestProtocol + "<\n";
        String theServletPath = request.getServletPath();
        theRequestInfo = theRequestInfo + " Servlet path = >" + theServletPath + "<\n";
        String thePathInfo = request.getPathInfo();
        theRequestInfo = theRequestInfo + " Path info = >" + thePathInfo + "<\n";
        String thePathTranslated = request.getPathTranslated();
        theRequestInfo = theRequestInfo + " Path translated = >" + thePathTranslated + "<\n";
        String theQueryString = request.getQueryString();
        theRequestInfo = theRequestInfo + " Query string = >" + theQueryString + "<\n";
        String theContentLength = Integer.toString(request.getContentLength());
        theRequestInfo = theRequestInfo + " Content length = >" + theContentLength + "<\n";
        String theContentType = request.getContentType();
        theRequestInfo = theRequestInfo + " Content type = >" + theContentType + "<\n";
        String theServerName = request.getServerName();
        theRequestInfo = theRequestInfo + " Server name = >" + theServerName + "<\n";
        String theServerPort = Integer.toString(request.getServerPort());
        theRequestInfo = theRequestInfo + " Server port = >" + theServerPort + "<\n";
        String theRemoteUser = request.getRemoteUser();
        theRequestInfo = theRequestInfo + " Remote user = " + theRemoteUser + "<\n";
        String theRemoteAddress = request.getRemoteAddr();
        theRequestInfo = theRequestInfo + " Remote address = >" + theRemoteAddress + "<\n";
        String theRemoteHost = request.getRemoteHost();
        theRequestInfo = theRequestInfo + " Remote host = >" + theRemoteHost + "<\n";
        String theAuthorizationScheme = request.getAuthType();
        theRequestInfo = theRequestInfo + " Authorization scheme = >" + theAuthorizationScheme + "<\n";
        for(Enumeration parameterNames = request.getParameterNames(); parameterNames.hasMoreElements();)
        {
            String aParameterName = (String)parameterNames.nextElement();
            String parameterValues[] = request.getParameterValues(aParameterName);
            int numberOfParameterValuesReturned = parameterValues.length;
            for(int i = 0; i < numberOfParameterValuesReturned; i++)
            {
                String currentLine = " parameter = >" + aParameterName + "<, value[" + i + "] = >" + parameterValues[i] + "<";
                theRequestInfo = theRequestInfo + currentLine + "\n";
            }

        }

        return theRequestInfo;
    }

    public static String dumpSessionContents(HttpServletRequest request)
    {
        String theContents = "";
        HttpSession session = request.getSession(false);
        if(session != null)
        {
            String theValueNames[] = session.getValueNames();
            int numberOfValues = theValueNames.length;
            for(int i = 0; i < numberOfValues; i++)
            {
                String currentValueName = theValueNames[i];
                Object theObject = session.getValue(currentValueName);
                theContents = theContents + currentValueName + "=>" + theObject + "<=\n";
            }

        } else
        {
            theContents = theContents + "<the session is null>";
        }
        return theContents;
    }

    public static String convertirDoubleAStringConRoundingYPunto(double valor, int numeroCifrasDespuesDelPunto)
    {
        double valorRounded = round(valor, numeroCifrasDespuesDelPunto);
        String stringValor = Double.toString(valorRounded) + "             ";
        int indice = stringValor.indexOf(".");
        String parteIzqValor = stringValor.substring(0, indice);
        String parteDerValor = stringValor.substring(indice, indice + numeroCifrasDespuesDelPunto + 1);
        stringValor = parteIzqValor + parteDerValor;
        return stringValor;
    }

    public static double round(double valor, int numeroCifrasDespuesDelPunto)
    {
        double cifrasDoubleVal = (new Integer(numeroCifrasDespuesDelPunto)).doubleValue();
        double factor = Math.pow(10D, cifrasDoubleVal);
        double valorMultiplicado = valor * factor;
        long multiplicadoYRounded = Math.round(valorMultiplicado);
        double valorRounded = (double)multiplicadoYRounded / factor;
        return valorRounded;
    }

    public static float round(float valor, int numeroCifrasDespuesDelPunto)
    {
        Float valorObj = new Float(valor);
        double rounded = round(valorObj.doubleValue(), numeroCifrasDespuesDelPunto);
        Float roundedObj = new Float(rounded);
        return roundedObj.floatValue();
    }

    public static int indiceDePrimerDigito(String esteString)
    {
        char charArray[] = esteString.toCharArray();
        boolean stopChecking = false;
        int indiceDelDigito = -1;
        for(int i = 0; i < charArray.length && !stopChecking; i++)
            if(Character.isDigit(charArray[i]))
            {
                indiceDelDigito = i;
                stopChecking = true;
            }

        return indiceDelDigito;
    }

    public static int validaContenidoLetraDigito(String esteString)
    {
        char charArray[] = esteString.toCharArray();
        boolean stopChecking = false;
        int indiceDelDigito = -1;
        for(int i = 0; i < charArray.length && !stopChecking; i++)
            if(!Character.isLetterOrDigit(charArray[i]))
            {
                indiceDelDigito = 0;
                stopChecking = true;
            }

        return indiceDelDigito;
    }

    public static int validaContenidoLetra(String esteString)
    {
        char charArray[] = esteString.toCharArray();
        boolean stopChecking = false;
        int indiceDelDigito = 1;
        for(int i = 0; i < charArray.length && !stopChecking; i++)
            if(!Character.isLetter(charArray[i]))
            {
                indiceDelDigito = 0;
                stopChecking = true;
            }

        return indiceDelDigito;
    }

    public static int validaContenidoNumero(String esteString)
    {
        char charArray[] = esteString.toCharArray();
        boolean stopChecking = false;
        int indiceDelDigito = 1;
        for(int i = 0; i < charArray.length && !stopChecking; i++)
            if(!Character.isDigit(charArray[i]))
            {
                indiceDelDigito = 0;
                stopChecking = true;
            }

        return indiceDelDigito;
    }

    public static String devolverStringSinPuntoSiNoHayDecimal(float valor)
    {
        String resultado = "";
        int intValor = (new Float(valor)).intValue();
        float remainder = valor - (float)intValor;
        if(remainder > 0.0F)
            resultado = String.valueOf(valor);
        else
            resultado = String.valueOf(intValor);
        return resultado;
    }

    public static String armarListaConComasYComillasDesdeStrings(Vector lista)
    {
        String listaString = "";
        if(lista != null)
        {
            for(int i = 0; i < lista.size(); i++)
            {
                String elemento = (String)lista.elementAt(i);
                listaString = listaString + "'" + elemento + "'";
                if(i != lista.size() - 1)
                    listaString = listaString + ",";
            }

        }
        return listaString;
    }

    public static String armarListaConComasDesdeIntegeres(Vector lista)
    {
        String listaString = "";
        if(lista != null)
        {
            for(int i = 0; i < lista.size(); i++)
            {
                Integer elemento = (Integer)lista.elementAt(i);
                listaString = listaString + elemento;
                if(i != lista.size() - 1)
                    listaString = listaString + ",";
            }

        }
        return listaString;
    }

    public static String completarConCerosIzq(String campo, int size)
    {
        String result = campo;
        int largo = campo.length();
        for(int i = largo; i < size; i++)
            result = "0" + result;

        return result;
    }

    public static BigDecimal convertircantidadAPorcentaje(BigDecimal pmCantidad, BigDecimal pmTotal)
    {
        BigDecimal resultado = null;
        BigDecimal cien = null;
        try
        {
            cien = new BigDecimal("100");
        }
        catch(NumberFormatException _ex) { }
        BigDecimal resultadoParcial = pmCantidad.multiply(cien);
        try
        {
            System.out.println("SedaUtils::convertircantidadAPorcentaje()::se va a hacer la division");
            resultado = resultadoParcial.divide(pmTotal, 1, 4);
            System.out.println("Resultado =" + resultadoParcial + "=");
        }
        catch(ArithmeticException _ex)
        {
            System.out.println("SedaUtils::convertircantidadAPorcentaje()::division por 0 o la escala es incorrecta");
        }
        catch(IllegalArgumentException _ex)
        {
            System.out.println("SedaUtils::convertircantidadAPorcentaje()::el modo de redondeo es incorrecto");
        }
        return resultado;
    }

    public static String getFechaHoyString()
    {
        Date hoy = new Date();
        int diaHoy = hoy.getDate();
        int mesHoy = hoy.getMonth() + 1;
        int anioHoy = hoy.getYear() + 1900;
        String diaHoyString = diaHoy + "/" + mesHoy + "/" + anioHoy;
        return diaHoyString;
    }

    public static String getPrimerDiaDelMes()
    {
        String mesHoyString = "";
        String diaHoyString = "";
        Date hoy = new Date();
        int dia = 1;
        int mes = hoy.getMonth() + 1;
        int anio = hoy.getYear() + 1900;
        if(dia < 10)
            diaHoyString = "0" + dia;
        else
            diaHoyString = String.valueOf(dia);
        if(mes < 10)
            mesHoyString = "0" + mes;
        else
            mesHoyString = String.valueOf(mes);
        String fecha = diaHoyString + "/" + mesHoyString + "/" + anio;
        return fecha;
    }

    public static String getUltimoDiaDelMes()
    {
        String mesHoyString = "";
        String diaHoyString = "";
        Date hoy = new Date();
        int dia = getDaysInMonth();
        int mes = hoy.getMonth() + 1;
        int anio = hoy.getYear() + 1900;
        if(dia < 10)
            diaHoyString = "0" + dia;
        else
            diaHoyString = String.valueOf(dia);
        if(mes < 10)
            mesHoyString = "0" + mes;
        else
            mesHoyString = String.valueOf(mes);
        String fecha = diaHoyString + "/" + mesHoyString + "/" + anio;
        return fecha;
    }

    public static int getDaysInMonth()
    {
        int days = 0;
        Date hoy = new Date();
        int month = hoy.getMonth() + 1;
        int year = hoy.getYear() + 1900;
        if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
            days = 31;
        else
        if(month == 4 || month == 6 || month == 9 || month == 11)
            days = 30;
        else
        if(month == 2)
            if(bisiesto(year))
                days = 29;
            else
                days = 28;
        return days;
    }

    public static boolean bisiesto(int Year)
    {
        return Year % 4 == 0 && Year % 100 != 0 || Year % 400 == 0;
    }

    public static String[] getMesAnio()
    {
        Date hoy = new Date();
        String resultado[] = new String[2];
        int mesActual = hoy.getMonth() + 1;
        int mesAnterior = 0;
        mesAnterior = mesActual - 1;
        if(mesAnterior > 0)
        {
            System.out.println("MISMO ANIO:");
            System.out.println("MES - 1:" + mesAnterior);
            resultado[0] = String.valueOf(mesAnterior);
            int anioHoy = hoy.getYear() + 1900;
            System.out.println("ANIO:" + anioHoy);
            resultado[1] = String.valueOf(anioHoy);
        }
        if(mesAnterior == 0)
        {
            System.out.println("DISTINTO ANIO:");
            resultado[0] = "12";
            int anioHoy = (hoy.getYear() + 1900) - 1;
            System.out.println("ANIO RESTADO:" + anioHoy);
            resultado[1] = String.valueOf(anioHoy);
        }
        return resultado;
    }

    public static String getFechaHoyStringConPunto()
    {
        Date hoy = new Date();
        int diaHoy = hoy.getDate();
        int mesHoy = hoy.getMonth() + 1;
        int anioHoy = hoy.getYear() + 1900;
        String diaHoyString = diaHoy + "." + mesHoy + "." + anioHoy;
        return diaHoyString;
    }

    public static String convertirFechaAString(String pmFecha)
    {
        StringTokenizer miTokenizer = new StringTokenizer(pmFecha, "/");
        String dia = miTokenizer.nextToken();
        String mes = miTokenizer.nextToken();
        String anio = miTokenizer.nextToken();
        String fecha = dia + "." + mes + "." + anio;
        return fecha;
    }

    public static String convertirFechaAStringOracle(String pmFecha)
    {
        StringTokenizer miTokenizer = new StringTokenizer(pmFecha, "/");
        String dia = miTokenizer.nextToken();
        String mes = miTokenizer.nextToken();
        String anio = miTokenizer.nextToken();
        String fecha = anio + mes + dia;
        return fecha;
    }

    public static String darFechaSinCeroIzquierda(String pmFecha)
    {
        StringTokenizer miTokenizer = new StringTokenizer(pmFecha, "/");
        String dia = miTokenizer.nextToken();
        String mes = miTokenizer.nextToken();
        String anio = miTokenizer.nextToken();
        if(dia.length() == 2)
        {
            String aux = dia.substring(0, 1);
            System.out.println(aux);
            if(aux.equals("0"))
            {
                dia = dia.substring(1, 2);
                System.out.println(dia);
            }
        }
        if(mes.length() == 2)
        {
            String aux1 = mes.substring(0, 1);
            if(aux1.equals("0"))
                mes = mes.substring(1, 2);
        }
        String fecha = dia + "/" + mes + "/" + anio;
        return fecha;
    }

    public static Date getFechaHoyDate()
    {
        Date hoy = new Date();
        int diaHoy = hoy.getDate();
        int mesHoy = hoy.getMonth();
        int anioHoy = hoy.getYear();
        Date fechaHoy = new Date(anioHoy, mesHoy, diaHoy);
        return fechaHoy;
    }

    public static Date getFechaHoyDateMasUno()
    {
        Date hoy = new Date();
        int diaHoy = hoy.getDate();
        int mesHoy = hoy.getMonth();
        int anioHoy = hoy.getYear();
        Date fechaHoy = new Date(anioHoy, mesHoy, diaHoy + 1);
        return fechaHoy;
    }

    public static Date convertirFechaADate(String pmFecha)
    {
        StringTokenizer miTokenizer = new StringTokenizer(pmFecha, "/");
        String dia = miTokenizer.nextToken();
        String mes = miTokenizer.nextToken();
        String anio = miTokenizer.nextToken();
        int diaInt = Integer.parseInt(dia);
        int mesInt = Integer.parseInt(mes);
        int anioInt = Integer.parseInt(anio);
        Date fecha = new Date(anioInt - 1900, mesInt - 1, diaInt);
        return fecha;
    }

    public static String getFechaToString(Date fecha)
    {
        int diaHoy = fecha.getDate();
        int mesHoy = fecha.getMonth() + 1;
        int anioHoy = fecha.getYear() + 1900;
        String diaHoyString = diaHoy + "/" + mesHoy + "/" + anioHoy;
        return diaHoyString;
    }

    public static String getFechaToStringConPunto(Date fecha)
    {
        int diaHoy = fecha.getDate();
        int mesHoy = fecha.getMonth() + 1;
        int anioHoy = fecha.getYear() + 1900;
        String diaHoyString = diaHoy + "." + mesHoy + "." + anioHoy;
        return diaHoyString;
    }

    public static String getFechaMananaString()
    {
        Date hoy = new Date();
        int diaHoy = hoy.getDate();
        int mesHoy = hoy.getMonth();
        int anioHoy = hoy.getYear();
        Date manana = new Date(anioHoy, mesHoy, diaHoy + 1);
        int diaManana = manana.getDate();
        int mesManana = manana.getMonth() + 1;
        int anioManana = manana.getYear() + 1900;
        String diaMananaString = diaManana + "/" + mesManana + "/" + anioManana;
        return diaMananaString;
    }

    public static Date getFechaMananaDate()
    {
        Date hoy = new Date();
        int diaHoy = hoy.getDate();
        int mesHoy = hoy.getMonth();
        int anioHoy = hoy.getYear();
        Date manana = new Date(anioHoy, mesHoy, diaHoy + 1);
        return manana;
    }

    public static Date getFechaMananaDateMasUno()
    {
        Date hoy = new Date();
        int diaHoy = hoy.getDate();
        int mesHoy = hoy.getMonth();
        int anioHoy = hoy.getYear();
        Date manana = new Date(anioHoy, mesHoy, diaHoy + 2);
        return manana;
    }

    public static String getDiaPosteriorString(int pmAnio, int pmMes, int pmDia)
    {
        Date fechaPosterior = new Date(pmAnio - 1900, pmMes - 1, pmDia + 1);
        int diaPosterior = fechaPosterior.getDate();
        int mesPosterior = fechaPosterior.getMonth() + 1;
        int anioPosterior = fechaPosterior.getYear() + 1900;
        String fechaPosteriorString = diaPosterior + "/" + mesPosterior + "/" + anioPosterior;
        return fechaPosteriorString;
    }

    public static String[] vectorToArrayString(Vector pmDatos)
    {
        int tamanoVector = pmDatos.size();
        String datosString[] = new String[tamanoVector];
        for(int i = 0; i < tamanoVector; i++)
        {
            Vector vectorAuxiliar = (Vector)pmDatos.elementAt(i);
            int longVectorAux = vectorAuxiliar.size();
            String stringAuxiliar = (String)vectorAuxiliar.elementAt(0);
            for(int j = 1; j < longVectorAux; j++)
                stringAuxiliar = stringAuxiliar + ";" + (String)vectorAuxiliar.elementAt(j);

            datosString[i] = stringAuxiliar;
        }

        return datosString;
    }

    public static boolean esBigDecimalValido(String pmNumero)
    {
        BigDecimal numero;
        try
        {
            numero = (new BigDecimal(pmNumero.trim())).setScale(3, 5);
        }
        catch(NumberFormatException _ex)
        {
            return false;
        }
        return true;
    }

    public static BigDecimal convertirABigDecimal(String pmNumero)
    {
        BigDecimal numero = null;
        try
        {
            numero = new BigDecimal(pmNumero);
        }
        catch(NumberFormatException _ex) { }
        return numero;
    }

    public static boolean checkFormatInputNumber(String pnumero)
    {
        boolean isInteger;
        try
        {
            Double tax = new Double(pnumero);
            NumberFormat formatter = NumberFormat.getNumberInstance();
            formatter.setMaximumFractionDigits(2);
            formatter.setMinimumFractionDigits(2);
            NumberFormat.getCurrencyInstance();
            return isInteger = true;
        }
        catch(NumberFormatException _ex)
        {
            return isInteger = false;
        }
    }

    public static String formatInputNumber(String pnumero)
    {
        String numberFormat = "";
        try
        {
            Double tax = new Double(pnumero);
            NumberFormat formatter = NumberFormat.getNumberInstance();
            formatter.setMaximumFractionDigits(2);
            formatter.setMinimumFractionDigits(2);
            NumberFormat.getCurrencyInstance();
            return formatter.format(tax);
        }
        catch(NumberFormatException _ex)
        {
            return numberFormat = "";
        }
    }

    public static boolean chequearCamposIntegrantes(String fIntegrante1MasTipoTrabajador, String fIntegrante2MasTipoTrabajador, String fIntegrante3MasTipoTrabajador, String fIntegrante4MasTipoTrabajador, String fIntegrante5MasTipoTrabajador)
    {
        boolean resultado = true;
        if(!fIntegrante2MasTipoTrabajador.equals("") && fIntegrante1MasTipoTrabajador.equals(""))
            return resultado = false;
        if(!fIntegrante3MasTipoTrabajador.equals("") && (fIntegrante1MasTipoTrabajador.equals("") || fIntegrante2MasTipoTrabajador.equals("")))
            return resultado = false;
        if(!fIntegrante4MasTipoTrabajador.equals("") && (fIntegrante1MasTipoTrabajador.equals("") || fIntegrante2MasTipoTrabajador.equals("") || fIntegrante3MasTipoTrabajador.equals("")))
            return resultado = false;
        if(!fIntegrante5MasTipoTrabajador.equals("") && (fIntegrante1MasTipoTrabajador.equals("") || fIntegrante2MasTipoTrabajador.equals("") || fIntegrante3MasTipoTrabajador.equals("") || fIntegrante4MasTipoTrabajador.equals("")))
            return resultado = false;
        else
            return resultado;
    }

    public static boolean chequearTrabajadoresDuplicados(String titular, String integrante1, String integrante2, String integrante3, String integrante4, String integrante5)
    {
        boolean resultado = true;
        if(!titular.equals("") && (titular.equals(integrante1) || titular.equals(integrante2) || titular.equals(integrante3) || titular.equals(integrante4) || titular.equals(integrante5)))
            return resultado = false;
        if(!integrante1.equals("") && (integrante1.equals(integrante2) || integrante1.equals(integrante3) || integrante1.equals(integrante4) || integrante1.equals(integrante5)))
            return resultado = false;
        if(!integrante2.equals("") && (integrante2.equals(integrante3) || integrante2.equals(integrante4) || integrante2.equals(integrante5)))
            return resultado = false;
        if(!integrante3.equals("") && (integrante3.equals(integrante4) || titular.equals(integrante5)))
            return resultado = false;
        if(!integrante4.equals("") && integrante4.equals(integrante5))
            return resultado = false;
        if(!titular.equals("") && (titular.equals(integrante1) || titular.equals(integrante2) || titular.equals(integrante3) || titular.equals(integrante4) || titular.equals(integrante5)))
            return resultado = false;
        else
            return resultado;
    }

    public static String formatearFecha(String pmFecha)
    {
        if(pmFecha.length() >= 10)
        {
            String anio = pmFecha.substring(0, 4);
            String mes = pmFecha.substring(5, 7);
            String dia = pmFecha.substring(8, 10);
            String fechaFormateada = dia + "/" + mes + "/" + anio;
            if(anio.equals("2999"))
                fechaFormateada = " ";
            return fechaFormateada;
        } else
        {
            return "";
        }
    }

    public static String formatearHora(String pmHora)
    {
        if(pmHora.length() >= 18)
        {
            String hora = pmHora.substring(11, 13);
            String minuto = pmHora.substring(14, 16);
            String horaFormateada = hora + ":" + minuto;
            return horaFormateada;
        } else
        {
            return "";
        }
    }

    public static String formatearFechaAAAAMMDD(String pmFecha)
    {
        String dia = pmFecha.substring(8, 10);
        String mes = pmFecha.substring(5, 7);
        String anio = pmFecha.substring(0, 4);
        String fechaFormateada = anio + mes + dia;
        return fechaFormateada;
    }

    public static String formatearFechaDDMMYYYY(String pmFecha)
    {
        StringTokenizer miTokenizer = new StringTokenizer(pmFecha, "/");
        String dia = miTokenizer.nextToken();
        String mes = miTokenizer.nextToken();
        String anio = miTokenizer.nextToken();
        if(dia.length() == 1)
            dia = "0" + dia;
        if(mes.length() == 1)
            mes = "0" + mes;
        String fecha = dia + "/" + mes + "/" + anio;
        return fecha;
    }

    public static String formatearFechaHoyDDMMYYYY()
    {
        String mesHoyString = "";
        String diaHoyString = "";
        Date hoy = new Date();
        int diaHoy = hoy.getDate();
        int mesHoy = hoy.getMonth() + 1;
        int anioHoy = hoy.getYear() + 1900;
        if(diaHoy < 10)
            diaHoyString = "0" + diaHoy;
        else
            diaHoyString = String.valueOf(diaHoy);
        if(mesHoy < 10)
            mesHoyString = "0" + mesHoy;
        else
            mesHoyString = String.valueOf(mesHoy);
        String fechaHoyString = diaHoyString + "/" + mesHoyString + "/" + anioHoy;
        return fechaHoyString;
    }

    public Utils()
    {
    }
}
