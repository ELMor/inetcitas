// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   VStat.java

package com.ssi.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Hashtable;
import java.util.Properties;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;

public class VStat
{

    public static final String databaseServer = "localhost";
    public static final int attempts = 3;
    public static final int waitTime = 1;
    public static final String tipoBase = "Oracle";
    public static String urlDB;
    public static String urlDriver;
    public static final String userName;
    public static final String password;
    public static final String url;
    public static final String driver;
    public static final int maxCon;
    public static final String nombreDeArchivoINI = "CitasParametros.properties";
    public static int maxConStatic;
    public static String usernameStatic;
    public static String passwordStatic;
    public static String host = "10.111.96.112";
    public static int port = 1521;
    public static String domain = "uwib5402";
    public static String sender = "lsanchez@ar.soluziona.com";
    public static String recipients = "lsanchez@ar.soluziona.com";

    public static void inicializarVariablesEstaticas()
    {
        Properties defaultSettings = setDefaults();
        Properties settings = leerArchivoIni(defaultSettings);
        setVariables(settings);
    }

    static Properties leerArchivoIni(Properties defaultSettings)
    {
        Properties settings = null;
        File file = new File(nombreDeArchivoINI);
        if(!file.exists())
            try
            {
                System.out.println("Creando archivo de configuracion "+nombreDeArchivoINI);
                FileOutputStream fos = new FileOutputStream(file);
                settings = defaultSettings;
                PrintStream ps = null;
                ps = new PrintStream(fos);
                ps.println("urlDB=" + settings.getProperty("urlDB").trim());
                ps.println("urlDriver=" + settings.getProperty("urlDriver").trim());
                ps.println("maxConStatic=" + Integer.parseInt(settings.getProperty("maxCon").trim()));
                ps.println("username=" + settings.getProperty("username").trim());
                ps.println("password=" + settings.getProperty("password").trim());
                ps.println(" ");
                ps.println(" ");
                fos.flush();
                fos.close();
            }
            catch(Exception e)
            {
                System.out.println("Error en escritura de archivo");
                System.err.println(e);
            }
        else
            try
            {
                FileInputStream fis = new FileInputStream(file);
                settings = new Properties(defaultSettings);
                settings.load(fis);
            }
            catch(Exception e)
            {
                System.out.println("Error en escritura de archivo");
                System.err.println(e);
            }
        return settings;
    }

    static void setVariables(Properties settings)
    {
        urlDB = settings.getProperty("urlDB");
        urlDriver = settings.getProperty("urlDriver");
        maxConStatic = Integer.parseInt(settings.getProperty("maxCon"));
        usernameStatic = settings.getProperty("username");
        passwordStatic = settings.getProperty("password");
    }

    public static Properties setDefaults()
    {
        Properties defaultSettings = new Properties();
        defaultSettings.put("urlDB",     "jdbc:oracle:thin:@10.111.96.112:1521:lx01");
        defaultSettings.put("urlDriver", "oracle.jdbc.driver.OracleDriver");
        defaultSettings.put("maxCon",    "10");
        defaultSettings.put("password",  "cla");
        defaultSettings.put("username",  "cla");

        return defaultSettings;
    }

    public VStat()
    {
    }

    static
    {
        inicializarVariablesEstaticas();
        url = urlDB;
        driver = urlDriver;
        maxCon = maxConStatic;
        userName = usernameStatic;
        password = passwordStatic;
    }
}
