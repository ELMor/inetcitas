// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   ProfesionalFichaDAO.java

package com.ssi.citas.profesional;

import com.ssi.bd.PoolManager;
import com.ssi.util.Utils;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

// Referenced classes of package com.ssi.citas.profesional:
//            ProfesionalFicha

public class ProfesionalFichaDAO
{

    static String mensaje = "";

    public ProfesionalFichaDAO()
    {
    }

    public Vector obtenerFichaProfesional(String pcodigoprofesional)
    {
        Vector resultado = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = obtenerQueryFicha(pcodigoprofesional);
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            ProfesionalFicha fichaProfesional;
            for(ResultSet set = stmt.executeQuery(query); set.next(); resultado.addElement(fichaProfesional))
            {
                fichaProfesional = new ProfesionalFicha();
                fichaProfesional.setNombre(set.getString(1));
                fichaProfesional.setApellido1(set.getString(2));
                fichaProfesional.setApellido2(set.getString(3));
                fichaProfesional.setNomCateg(set.getString(4));
                fichaProfesional.setCodigoColegiado(set.getString(5));
                fichaProfesional.setDelCentroSn(set.getString(6));
                fichaProfesional.setCodigoPersona(set.getString(7));
                fichaProfesional.setActivoSn(set.getString(8));
                fichaProfesional.setProfConAgenSn(set.getString(9));
                fichaProfesional.setCodPerso(set.getString(10));
                fichaProfesional.setCodigoInterno(set.getString(11));
                fichaProfesional.setTipoDniDesc(set.getString(12));
                fichaProfesional.setFpersDoc(set.getString(13));
                fichaProfesional.setGrpmedDetTit(set.getString(14));
                fichaProfesional.setGrpmed_cab_desc(set.getString(15));
                fichaProfesional.setNombre_ld1(set.getString(16));
                fichaProfesional.setNombre_ld2(set.getString(17));
                fichaProfesional.setNombre_ld3(set.getString(18));
                fichaProfesional.setNombre_ld4(set.getString(19));
                fichaProfesional.setNombre_ld5(set.getString(20));
                fichaProfesional.setCpostal(set.getString(21));
                fichaProfesional.setFpersDomVia(set.getString(22));
                fichaProfesional.setFpersDomMun(set.getString(23));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            Utils.enviarMail(s.toString());
            s.printStackTrace();
            setMensaje(s.toString());
            try{
            conn.rollback();
            conn.setAutoCommit(true);
            }catch(Exception e){
            }
        }
        finally
        {
            pool.push(conn);
            return resultado;
        }
    }

    public static String getMensaje()
    {
        return mensaje;
    }

    public static void setMensaje(String vmensaje)
    {
        mensaje = vmensaje;
    }

    public String obtenerQueryFicha(String pcodigoProfesional)
    {
        String result = "";
        String codigoprofesional = pcodigoProfesional.trim();
        result = "SELECT fpersona.nombre,fpersona.apellido1,fpersona.apellido2,tcategor.nom_categ,fpersona.codigo_colegiado,fpersona.del_centro_sn,fpersona.codigo_persona,fpersona.activo_sn,fpersona.prof_con_agen_sn,fpersona.cod_perso,fpersona.codigo_interno,tpo_dni.tipo_dni_desc,fpersona.fpers_doc,grupo_med_det.grpmed_det_tit,grupo_med_cab.grpmed_cab_desc,ld1.nombre_ld1,ld2.nombre_ld2,ld3.nombre_ld3,ld4.nombre_ld4,ld5.nombre_ld5,ld5.cpostal,fpersona.fpers_dom_via,fpersona.fpers_dom_mun FROM fpersona, ld1, ld2, ld3, ld4, ld5, tcategor, tpo_dni, grupo_med_det,grupo_med_cab WHERE tcategor.codigo_categoria (+) = fpersona.codigo_categoria and tpo_dni.tipo_dni_pk (+) = fpersona.tipo_dni_pk and grupo_med_det.codigo_personal(+) = fpersona.codigo_personal and grupo_med_cab.grpmed_cab_pk (+) = grupo_med_det.grpmed_cab_pk and ld1.codigo_ld1 (+) = fpersona.fpers_dom_ld1 and ld2.codigo_ld2 (+) = fpersona.fpers_dom_ld2 and ld3.codigo_ld3 (+) = fpersona.fpers_dom_ld3 and ld4.codigo_ld4 (+) = fpersona.fpers_dom_ld4 and ld5.codigo_ld5 (+) = fpersona.fpers_dom_ld5 and fpersona.codigo_personal = '" + codigoprofesional + "' ";
        return result;
    }

    public Vector obtenerListaProfesionales(String pcodigoEspecialidad)
    {
        Vector resultado = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = obtenerQueryListaProfesionales(pcodigoEspecialidad);
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            ProfesionalFicha fichaProfesional;
            for(ResultSet set = stmt.executeQuery(query); set.next(); resultado.addElement(fichaProfesional))
            {
                fichaProfesional = new ProfesionalFicha();
                fichaProfesional.setCodigoPersona(set.getString(1));
                fichaProfesional.setNombre(set.getString(2));
                fichaProfesional.setApellido1(set.getString(3));
                fichaProfesional.setApellido2(set.getString(4));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            s.printStackTrace();
            setMensaje(s.toString());
            try{
            conn.rollback();
            conn.setAutoCommit(true);
            }catch(Exception e){
            }
        }
        finally
        {
            pool.push(conn);
            return resultado;
        }
    }

    public String obtenerQueryListaProfesionales(String pcodigoEspecialidad)
    {
        String result = "";
        result = "Select fpersona.codigo_personal,fpersona.nombre,fpersona.apellido1,fpersona.apellido2 from fpersona, servicios, fpersona_servicio where servicios.codigo_servicio = fpersona_servicio.codigo_servicio and fpersona_servicio.codigo_personal = fpersona.codigo_personal and fpersona.prof_con_agen_sn = 1 and fpersona.activo_sn = 1 and servicios.gfh_pk = '" + pcodigoEspecialidad.trim() + "' " + "ORDER BY fpersona.apellido1 asc";
        return result;
    }

}
