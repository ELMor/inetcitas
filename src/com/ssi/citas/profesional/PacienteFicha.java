// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   PacienteFicha.java

package com.ssi.citas.profesional;


public class PacienteFicha
{

    String codigo;
    String apellido1;
    String apellido2;
    String nombre;
    String tipo_dni_desc;
    String codigo1;
    String nac_fecha;
    String nombre_ld1;
    String nombre_ld2;
    String nombre_ld3;
    String nombre_ld4;
    String nombre_ld5;
    String telefono1;
    String telefono2;
    String nhc;
    String nombre_sexo;
    String estado_civil;
    String pac_peso;
    String pac_talla;
    String pac_pesoFecha;
    String pac_tallaFecha;
    String pacDomDireccion;
    String desFactor;
    String desGrupo;
    String cpostal;

    public PacienteFicha()
    {
        codigo = "";
        apellido1 = "";
        apellido2 = "";
        nombre = "";
        tipo_dni_desc = "";
        nombre_ld1 = "";
        nombre_ld2 = "";
        nombre_ld3 = "";
        nombre_ld4 = "";
        nombre_ld5 = "";
        codigo1 = "";
        nac_fecha = "";
        telefono1 = "";
        telefono2 = "";
        nhc = "";
        nombre_sexo = "";
        estado_civil = "";
        pac_peso = "";
        pac_talla = "";
        pac_pesoFecha = "";
        pac_tallaFecha = "";
        pacDomDireccion = "";
        desFactor = "";
        desGrupo = "";
        cpostal = "";
    }

    public String getDesGrupo()
    {
        return desGrupo;
    }

    public void setDesGrupo(String pdesGrupo)
    {
        desGrupo = pdesGrupo;
    }

    public String getCpostal()
    {
        return cpostal;
    }

    public void setCpostal(String pcpostal)
    {
        cpostal = pcpostal;
    }

    public String getDesFactor()
    {
        return desFactor;
    }

    public void setDesFactor(String pdesFactor)
    {
        desFactor = pdesFactor;
    }

    public String getPacDomDireccion()
    {
        return pacDomDireccion;
    }

    public void setPacDomDireccion(String ppacDomDireccion)
    {
        pacDomDireccion = ppacDomDireccion;
    }

    public String getTelefono1()
    {
        return telefono1;
    }

    public void setTelefono1(String ptelefono1)
    {
        telefono1 = ptelefono1;
    }

    public String getNacFecha()
    {
        return nac_fecha;
    }

    public void setNacFecha(String pnac_fecha)
    {
        nac_fecha = pnac_fecha;
    }

    public String getTelefono2()
    {
        return telefono2;
    }

    public void setTelefono2(String ptelefono2)
    {
        telefono2 = ptelefono2;
    }

    public String getNHC()
    {
        return nhc;
    }

    public void setNHC(String pnhc)
    {
        nhc = pnhc;
    }

    public String getNombreSexo()
    {
        return nombre_sexo;
    }

    public void setNombreSexo(String pnombre_sexo)
    {
        nombre_sexo = pnombre_sexo;
    }

    public String getEstadoCivil()
    {
        return estado_civil;
    }

    public void setEstadoCivil(String pestado_civil)
    {
        estado_civil = pestado_civil;
    }

    public String getPacPeso()
    {
        return pac_peso;
    }

    public void setPacPeso(String ppac_peso)
    {
        pac_peso = ppac_peso;
    }

    public String getPacPesoFecha()
    {
        return pac_pesoFecha;
    }

    public void setPacPesoFecha(String ppac_pesoFecha)
    {
        pac_pesoFecha = ppac_pesoFecha;
    }

    public String getPacTalla()
    {
        return pac_talla;
    }

    public void setPacTalla(String ppac_talla)
    {
        pac_talla = ppac_talla;
    }

    public String getPacTallaFecha()
    {
        return pac_tallaFecha;
    }

    public void setPacTallaFecha(String ppac_tallaFecha)
    {
        pac_tallaFecha = ppac_tallaFecha;
    }

    public String getTipoDniDesc()
    {
        return tipo_dni_desc;
    }

    public void setTipoDniDesc(String ptipo_dni_desc)
    {
        tipo_dni_desc = ptipo_dni_desc;
    }

    public String getNombre_ld1()
    {
        return nombre_ld1;
    }

    public void setNombre_ld1(String pnombre_ld1)
    {
        nombre_ld1 = pnombre_ld1;
    }

    public String getNombre_ld2()
    {
        return nombre_ld2;
    }

    public void setNombre_ld2(String pnombre_ld2)
    {
        nombre_ld2 = pnombre_ld2;
    }

    public String getNombre_ld3()
    {
        return nombre_ld3;
    }

    public void setNombre_ld3(String pnombre_ld3)
    {
        nombre_ld3 = pnombre_ld3;
    }

    public String getNombre_ld4()
    {
        return nombre_ld4;
    }

    public void setNombre_ld4(String pnombre_ld4)
    {
        nombre_ld4 = pnombre_ld4;
    }

    public String getNombre_ld5()
    {
        return nombre_ld5;
    }

    public void setNombre_ld5(String pnombre_ld5)
    {
        nombre_ld5 = pnombre_ld5;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String vcodigo)
    {
        codigo = vcodigo;
    }

    public String getCodigo1()
    {
        return codigo1;
    }

    public void setCodigo1(String vcodigo1)
    {
        codigo1 = vcodigo1;
    }

    public String getApellido1()
    {
        return apellido1;
    }

    public void setApellido1(String vapellido1)
    {
        apellido1 = vapellido1;
    }

    public String getApellido2()
    {
        return apellido2;
    }

    public void setApellido2(String vapellido2)
    {
        apellido2 = vapellido2;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String vnombre)
    {
        nombre = vnombre;
    }
}
