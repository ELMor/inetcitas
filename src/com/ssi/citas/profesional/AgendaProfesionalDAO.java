// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   AgendaProfesionalDAO.java

package com.ssi.citas.profesional;

import com.ssi.bd.PoolManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

// Referenced classes of package com.ssi.citas.profesional:
//            AgendaProfesional

public class AgendaProfesionalDAO
    implements Cloneable
{

    static String mensaje = "";

    public AgendaProfesionalDAO()
    {
    }

    public static String getMensaje()
    {
        return mensaje;
    }

    public static void setMensaje(String vmensaje)
    {
        mensaje = vmensaje;
    }

    public Vector obteneraAgendaProfesional(String pcodigoprofesional, String pfechadesde, String pfechahasta, String pcentro, String porden, String pespecialidad)
    {
        Vector resultado = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = obtenerQueryAgenda(pcodigoprofesional, pfechadesde, pfechahasta, pcentro, porden, pespecialidad);
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            AgendaProfesional agendaProfesional;
            for(ResultSet set = stmt.executeQuery(query); set.next(); resultado.addElement(agendaProfesional))
            {
                agendaProfesional = new AgendaProfesional();
                agendaProfesional.setFecha(set.getString(1));
                agendaProfesional.setHora(set.getString(2));
                agendaProfesional.setNombreCentro(set.getString(3));
                agendaProfesional.setDireccionCentro(set.getString(4));
                agendaProfesional.setServicio(set.getString(5));
                agendaProfesional.setApellido1Paciente(set.getString(6));
                agendaProfesional.setApellido2Paciente(set.getString(7));
                agendaProfesional.setNombrePaciente(set.getString(8));
                agendaProfesional.setEstado(set.getString(9));
                agendaProfesional.setTelefono1Paciente(set.getString(10));
                agendaProfesional.setCodigoPaciente(set.getString(11));
                agendaProfesional.setCodigoPlan(set.getString(12));
                agendaProfesional.setGarante(set.getString(13));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            s.printStackTrace();
            setMensaje(s.toString());
            try{
            conn.rollback();
            conn.setAutoCommit(true);
            }catch(Exception e){
            }
        }
        finally
        {
            pool.push(conn);
            return resultado;
        }
    }

    public String obtenerQueryAgenda(String pcodigoprofesional, String pfechadesde, String pfechahasta, String pcentro, String porden, String pespecialidad)
    {
        String codigoprofesional = pcodigoprofesional.trim();
        String fechadesde = pfechadesde.trim();
        String fechahasta = pfechahasta.trim();
        String centro = pcentro.trim();
        String orden = porden.trim();
        String especialidad = pespecialidad.trim();
        String where = "";
        String orderby = " cagendas.fecha desc";
        if(!centro.equals("S/D"))
            where = "And centros.cod_centro = '" + centro + "'";
        if(!especialidad.equals("S/D"))
            where = where + " And servicios.gfh_pk = '" + especialidad + "'";
        if(!orden.equals(""))
        {
            if(orden.equals("FechaAsc"))
                orderby = "cagendas.fecha asc";
            if(orden.equals("FechaDesc"))
                orderby = "cagendas.fecha desc";
            if(orden.equals("HoraAsc"))
                orderby = "cagendas.hora asc";
            if(orden.equals("HoraDesc"))
                orderby = "cagendas.hora desc";
            if(orden.equals("CentroAsc"))
                orderby = "centros.nombre_centro asc";
            if(orden.equals("CentroDesc"))
                orderby = "centros.nombre_centro desc";
            if(orden.equals("EspecialidadAsc"))
                orderby = "servicios.servicio asc";
            if(orden.equals("EspecialidadDesc"))
                orderby = "servicios.servicio desc";
            if(orden.equals("PacienteAsc"))
                orderby = "clientes.apellido1 asc,clientes.apellido2 asc,clientes.nombre asc";
            if(orden.equals("PacienteDesc"))
                orderby = "clientes.apellido1 desc,clientes.apellido2 desc,clientes.nombre desc";
            if(orden.equals("EstadoAsc"))
                orderby = "cagendas.cod_estado asc";
            if(orden.equals("EstadoDesc"))
                orderby = "cagendas.cod_estado desc";
            if(orden.equals("TelefonoAsc"))
                orderby = "clientes.telefono1 asc";
            if(orden.equals("TelefonoDesc"))
                orderby = "clientes.telefono1 desc";
            if(orden.equals("CodPacAsc"))
                orderby = "clientes.codigo_cliente asc";
            if(orden.equals("CodPacDesc"))
                orderby = "clientes.codigo_cliente desc";
            if(orden.equals("CodPlanAsc"))
                orderby = "co_planes.plan_desc asc";
            if(orden.equals("CodPlanDesc"))
                orderby = "co_planes.plan_desc desc";
        }
        String query = "SELECT cagendas.fecha, cagendas.hora, centros.nombre_centro,centros.ctro_direcc, servicios.servicio, clientes.apellido1,clientes.apellido2, clientes.nombre, cagendas.cod_estado,clientes.telefono1, clientes.codigo_cliente, co_planes.plan_desc, garantes.nombre_garante FROM cagendas, clientes, centros, servicios, co_planes, garantes Where clientes.codigo_cliente (+) = cagendas.codigo_cliente and centros.cod_centro = cagendas.cod_centro and cagendas.plan_pk = co_planes.plan_pk (+) and co_planes.codigo_garante_pk  = garantes.codigo_garante_pk (+) And servicios.codigo_servicio = cagendas.codigo_servicio and cagendas.codigo_personal = '" + codigoprofesional + "' " + "And cagendas.cod_estado in ('LI', 'CI', 'VI') " + " " + where + " " + "And cagendas.fecha >= to_date ('" + fechadesde + "', 'DD/MM/YYYY') " + "and cagendas.fecha <= to_date ('" + fechahasta + "', 'DD/MM/YYYY') " + "ORDER BY " + orderby;
        return query;
    }

}
