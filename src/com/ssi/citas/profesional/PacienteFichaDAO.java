// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   PacienteFichaDAO.java

package com.ssi.citas.profesional;

import com.ssi.bd.PoolManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

// Referenced classes of package com.ssi.citas.profesional:
//            PacienteFicha

public class PacienteFichaDAO
{

    static String mensaje = "";

    public PacienteFichaDAO()
    {
    }

    public Vector obtenerFichaPaciente(String pcodigopaciente)
    {
        Vector resultado = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = obtenerQueryFicha(pcodigopaciente);
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            PacienteFicha fichaPaciente;
            for(ResultSet set = stmt.executeQuery(query); set.next(); resultado.addElement(fichaPaciente))
            {
                fichaPaciente = new PacienteFicha();
                fichaPaciente.setApellido1(set.getString(1));
                fichaPaciente.setApellido2(set.getString(2));
                fichaPaciente.setNombre(set.getString(3));
                fichaPaciente.setTipoDniDesc(set.getString(4));
                fichaPaciente.setCodigo1(set.getString(5));
                fichaPaciente.setNacFecha(set.getString(6));
                fichaPaciente.setNombre_ld1(set.getString(7));
                fichaPaciente.setNombre_ld2(set.getString(8));
                fichaPaciente.setNombre_ld3(set.getString(9));
                fichaPaciente.setNombre_ld4(set.getString(10));
                fichaPaciente.setNombre_ld5(set.getString(11));
                fichaPaciente.setTelefono1(set.getString(12));
                fichaPaciente.setTelefono2(set.getString(13));
                fichaPaciente.setNHC(set.getString(14));
                fichaPaciente.setNombreSexo(set.getString(15));
                fichaPaciente.setEstadoCivil(set.getString(16));
                fichaPaciente.setPacPeso(set.getString(17));
                fichaPaciente.setPacPesoFecha(set.getString(18));
                fichaPaciente.setPacTalla(set.getString(19));
                fichaPaciente.setPacTallaFecha(set.getString(20));
                fichaPaciente.setPacDomDireccion(set.getString(21));
                fichaPaciente.setDesFactor(set.getString(22));
                fichaPaciente.setDesGrupo(set.getString(23));
                fichaPaciente.setCpostal(set.getString(24));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            s.printStackTrace();
            setMensaje(s.toString());
            try{
            conn.rollback();
            conn.setAutoCommit(true);
            }catch(Exception e){
            }
        }
        finally
        {
            pool.push(conn);
            return resultado;
        }
    }

    public static String getMensaje()
    {
        return mensaje;
    }

    public static void setMensaje(String vmensaje)
    {
        mensaje = vmensaje;
    }

    public String obtenerQueryFicha(String pcodigoPaciente)
    {
        String result = "";
        String codigopaciente = pcodigoPaciente.trim();
        result = "SELECT clientes.apellido1,clientes.apellido2,clientes.nombre,tpo_dni.tipo_dni_desc,clientes.codigo1,clientes.nac_fecha,ld1.nombre_ld1,ld2.nombre_ld2,ld3.nombre_ld3,ld4.nombre_ld4,ld5.nombre_ld5,clientes.telefono1,clientes.telefono2,hc.nhc,sexo.nombre_sexo,estado_civil.estado_civil,clientes.pac_peso,clientes.pac_peso_fecha,clientes.pac_talla,clientes.pac_talla_fecha,clientes.dom_direccion,bs_factor.des_factor,bs_grupo.des_grupo,ld5.cpostal FROM clientes, estado_civil, hc, ld1, ld2, ld3, ld4, ld5, sexo, tpo_dni, bs_factor, bs_grupo WHERE clientes.codigo_sexo = sexo.codigo_sexo (+) AND clientes.codigo_ecivil = estado_civil.codigo_ecivil (+) AND clientes.tipo_dni_pk = tpo_dni.tipo_dni_pk (+) AND clientes.codigo_cliente = hc.codigo_cliente (+) AND hc.activa_sn (+) = 1 AND clientes.dom_ld1 = ld1.codigo_ld1 (+) AND clientes.dom_ld2 = ld2.codigo_ld2 (+) AND clientes.dom_ld3 = ld3.codigo_ld3 (+) AND clientes.dom_ld4 = ld4.codigo_ld4 (+) AND clientes.dom_ld5 = ld5.codigo_ld5 (+) AND clientes.cod_factor = bs_factor.cod_factor (+) AND clientes.cod_grupo = bs_grupo.cod_grupo (+) AND clientes.codigo_cliente ='" + codigopaciente + "'";
        return result;
    }

}
