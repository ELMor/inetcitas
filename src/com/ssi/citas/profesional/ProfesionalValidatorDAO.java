// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   ProfesionalValidatorDAO.java

package com.ssi.citas.profesional;

import com.ssi.bd.PoolManager;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

// Referenced classes of package com.ssi.citas.profesional:
//            Profesional

public class ProfesionalValidatorDAO
{

    public String isValid(Profesional unProfesional)
    {
        String resultadoString = "TRUE";
        boolean tablaLogin = false;
        String query = getSelectQuery(unProfesional);
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet resultado = stmt.executeQuery(query);
            if(resultado.next())
            {
                unProfesional.setCodigo(resultado.getString(1));
                unProfesional.setApellido1(resultado.getString(2));
                unProfesional.setApellido2(resultado.getString(3));
                unProfesional.setNombre(resultado.getString(4));
            } else
            {
                resultadoString = "FALSE";
            }
        }
        catch(Exception s)
        {
            resultadoString = "FALSE";
            System.out.println(String.valueOf(s));
        }
        finally
        {
            pool.push(conn);
        }
        return resultadoString;
    }

    private String getSelectQuery(Profesional unProfesional)
    {
        String result = "";
        String usuario = unProfesional.getUsuario().trim();
        String password = unProfesional.getPassword().trim();
        result = "select sys_usu.codigo_personal,fpersona.apellido1,fpersona.apellido2, fpersona.nombre from sys_apli, sys_usap, sys_usu, fpersona where sys_apli.cod_apli = sys_usap.cod_apli and sys_usap.syslogin = sys_usu.syslogin and sys_usu.syslogin = '" + usuario + "' " + "and sys_usu.syspwd = '" + password + "' " + "and sys_usap.permiso = 1 " + "and sys_apli.nivel = 1 " + "and sys_apli.modulo = 'cita' " + "and sys_usu.codigo_personal = fpersona.codigo_personal ";
        return result;
    }

    public ProfesionalValidatorDAO()
    {
    }
}
