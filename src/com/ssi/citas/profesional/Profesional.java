// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   Profesional.java

package com.ssi.citas.profesional;


public class Profesional
{

    String codigo;
    String apellido1;
    String apellido2;
    String nombre;
    String usuario;
    String password;

    public Profesional()
    {
        codigo = "";
        apellido1 = "";
        apellido2 = "";
        nombre = "";
        usuario = "";
        password = "";
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String vcodigo)
    {
        codigo = vcodigo;
    }

    public String getUsuario()
    {
        return usuario;
    }

    public void setUsuario(String vusuario)
    {
        usuario = vusuario;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String vpassword)
    {
        password = vpassword;
    }

    public String getApellido1()
    {
        return apellido1;
    }

    public void setApellido1(String vapellido1)
    {
        apellido1 = vapellido1;
    }

    public String getApellido2()
    {
        return apellido2;
    }

    public void setApellido2(String vapellido2)
    {
        apellido2 = vapellido2;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String vnombre)
    {
        nombre = vnombre;
    }
}
