// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   ServicioDAO.java

package com.ssi.citas.comun;

import com.ssi.bd.PoolManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

// Referenced classes of package com.ssi.citas.comun:
//            Servicio

public class ServicioDAO
    implements Cloneable
{

    static String mensaje = "";

    public ServicioDAO()
    {
    }

    public static String getMensaje()
    {
        return mensaje;
    }

    public static void setMensaje(String vmensaje)
    {
        mensaje = vmensaje;
    }

    public Vector obtenerServicio(String pcodigoProfesional)
    {
        Vector resultado = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = "SELECT servicios.codigo_servicio,servicios.servicio FROM fpersona,fpersona_servicio,servicios WHERE fpersona_servicio.codigo_personal = fpersona.codigo_personal and servicios.codigo_servicio = fpersona_servicio.codigo_servicio and fpersona.codigo_personal ='" + pcodigoProfesional.trim() + "'";
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            Servicio servicio;
            for(ResultSet set = stmt.executeQuery(query); set.next(); resultado.addElement(servicio))
            {
                servicio = new Servicio();
                servicio.setCodigo(set.getString(1));
                servicio.setDescripcion(set.getString(2));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            s.printStackTrace();
            setMensaje(s.toString());
            try{
            conn.rollback();
            conn.setAutoCommit(true);
            }catch(Exception e){
            }
        }
        finally
        {
            pool.push(conn);
            return resultado;
        }
    }

}
