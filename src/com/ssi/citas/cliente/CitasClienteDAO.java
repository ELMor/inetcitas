// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   CitasClienteDAO.java

package com.ssi.citas.cliente;

import com.ssi.bd.PoolManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

// Referenced classes of package com.ssi.citas.cliente:
//            CitasCliente

public class CitasClienteDAO
    implements Cloneable
{

    static String mensaje = "";

    public CitasClienteDAO()
    {
    }

    public static String getMensaje()
    {
        return mensaje;
    }

    public static void setMensaje(String vmensaje)
    {
        mensaje = vmensaje;
    }

    public Vector obtenerCitasCliente(String pcodigocliente, String pfechadesde, String pfechahasta, String pestado, String porden)
    {
        Vector resultado = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = obtenerQueryCitas(pcodigocliente, pfechadesde, pfechahasta, pestado, porden);
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            CitasCliente citascliente;
            for(ResultSet set = stmt.executeQuery(query); set.next(); resultado.addElement(citascliente))
            {
                citascliente = new CitasCliente();
                citascliente.setFecha(set.getString(1));
                citascliente.setHora(set.getString(2));
                citascliente.setNombreCentro(set.getString(3));
                citascliente.setDireccionCentro(set.getString(4));
                citascliente.setServicio(set.getString(5));
                citascliente.setNombreCorto(set.getString(6));
                citascliente.setEstado(set.getString(7));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            s.printStackTrace();
            setMensaje(s.toString());
            try {
              conn.rollback();
              conn.setAutoCommit(true);
            }catch(Exception e){}
        }
        finally
        {
            pool.push(conn);
            return resultado;
        }
    }

    public String obtenerQueryCitas(String pcodigocliente, String pfechadesde, String pfechahasta, String pestado, String porden)
    {
        String codigocliente = pcodigocliente.trim();
        String fechadesde = pfechadesde.trim();
        String fechahasta = pfechahasta.trim();
        String estado = pestado.trim();
        String orden = porden.trim();
        String orderby = " cagendas.fecha desc";
        String where = "centros.cod_centro = cagendas.cod_centro and servicios.codigo_servicio = cagendas.codigo_servicio and fpersona.codigo_personal = cagendas.codigo_personal and cagendas.codigo_cliente = '" + codigocliente + "'";
        if(!orden.equals(""))
        {
            if(orden.equals("FechaAsc"))
                orderby = "cagendas.fecha asc,cagendas.hora asc";
            if(orden.equals("FechaDesc"))
                orderby = "cagendas.fecha desc,cagendas.hora desc";
            if(orden.equals("CentroAsc"))
                orderby = "centros.nombre_centro asc";
            if(orden.equals("CentroDesc"))
                orderby = "centros.nombre_centro desc";
            if(orden.equals("EspecialidadAsc"))
                orderby = "servicios.servicio asc";
            if(orden.equals("EspecialidadDesc"))
                orderby = "servicios.servicio desc";
            if(orden.equals("ProfesionalAsc"))
                orderby = "fpersona.nombre_corto asc";
            if(orden.equals("ProfesionalDesc"))
                orderby = "fpersona.nombre_corto desc";
            if(orden.equals("EstadoAsc"))
                orderby = "cagendas.cod_estado asc";
            if(orden.equals("EstadoDesc"))
                orderby = "cagendas.cod_estado desc";
        }
        if(!pestado.equals(""))
            if(pestado.equals("TODOS"))
                where = where + " and (cod_estado = 'CI' or cod_estado = 'VI')";
            else
                where = where + " and (cod_estado = '" + estado + "')";
        if(!fechadesde.equals("") && !fechahasta.equals(""))
            where = where + " and (fecha >= to_date('" + fechadesde + "','dd/mm/yyyy') and fecha <= to_date('" + fechahasta + "','dd/mm/yyyy'))";
        String query = "SELECT cagendas.fecha, cagendas.hora, centros.nombre_centro, centros.ctro_direcc, servicios.servicio, fpersona.nombre_corto, cagendas.cod_estado FROM cagendas, centros, servicios, fpersona WHERE " + where + "" + "ORDER BY " + orderby;
        return query;
    }

    public Vector obtenerCitasClienteDisponibles(String pcodigocliente, String pfechadesde, String pfechahasta, String pestado, String porden, String pcentro, String pespecialidad,
            String pprofesional)
    {
        Vector resultado = new Vector();
        PoolManager pool = PoolManager.getInstance();
        Connection conn = pool.pop();
        String query = obtenerQueryCitasDisponibles(pcodigocliente, pfechadesde, pfechahasta, pestado, porden, pcentro, pespecialidad, pprofesional);
        try
        {
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();
            CitasCliente citascliente;
            for(ResultSet set = stmt.executeQuery(query); set.next(); resultado.addElement(citascliente))
            {
                citascliente = new CitasCliente();
                citascliente.setFecha(set.getString(1));
                citascliente.setHora(set.getString(2));
                citascliente.setNombreCentro(set.getString(3));
                citascliente.setDireccionCentro(set.getString(4));
                citascliente.setServicio(set.getString(5));
                citascliente.setEstado(set.getString(6));
                citascliente.setNombreCorto(set.getString(7));
            }

            conn.commit();
            conn.setAutoCommit(true);
            stmt.close();
        }
        catch(Exception s)
        {
            s.printStackTrace();
            setMensaje(s.toString());
            try{
              conn.rollback();
              conn.setAutoCommit(true);
            }catch(Exception e){}
        }
        finally
        {
            pool.push(conn);
            return resultado;
        }
    }

    public String obtenerQueryCitasDisponibles(String pcodigocliente, String pfechadesde, String pfechahasta, String pestado, String porden, String pcentro, String pespecialidad,
            String pprofesional)
    {
        String codigocliente = pcodigocliente.trim();
        String fechadesde = pfechadesde.trim();
        String fechahasta = pfechahasta.trim();
        String estado = pestado.trim();
        String centro = pcentro.trim();
        String especialidad = pespecialidad.trim();
        String profesional = pprofesional.trim();
        String orden = porden.trim();
        String orderby = " cagendas.fecha desc";
        String where = "centros.cod_centro = cagendas.cod_centro and servicios.codigo_servicio = cagendas.codigo_servicio and fpersona.codigo_personal = cagendas.codigo_personal and cagendas.cod_estado = 'LI'";
        if(!centro.equals(""))
            where = where + " and '" + centro + "' = cagendas.cod_centro";
        if(!especialidad.equals(""))
            where = where + " and '" + especialidad + "' = cagendas.gfh_pk";
        if(!profesional.equals(""))
            where = where + " and '" + profesional + "' = cagendas.codigo_personal";
        if(!fechadesde.equals("") && !fechahasta.equals(""))
            where = where + " and (cagendas.fecha >= to_date('" + fechadesde + "','dd/mm/yyyy') and cagendas.fecha <= to_date('" + fechahasta + "','dd/mm/yyyy'))";
        if(!orden.equals(""))
        {
            if(orden.equals("FechaAsc"))
                orderby = "cagendas.fecha asc,cagendas.hora asc";
            if(orden.equals("FechaDesc"))
                orderby = "cagendas.fecha desc,cagendas.hora desc";
            if(orden.equals("CentroAsc"))
                orderby = "centros.nombre_centro asc";
            if(orden.equals("CentroDesc"))
                orderby = "centros.nombre_centro desc";
            if(orden.equals("EspecialidadAsc"))
                orderby = "servicios.servicio asc";
            if(orden.equals("EspecialidadDesc"))
                orderby = "servicios.servicio desc";
            if(orden.equals("ProfesionalAsc"))
                orderby = "fpersona.nombre_corto asc";
            if(orden.equals("ProfesionalDesc"))
                orderby = "fpersona.nombre_corto desc";
            if(orden.equals("EstadoAsc"))
                orderby = "cagendas.cod_estado asc";
            if(orden.equals("EstadoDesc"))
                orderby = "cagendas.cod_estado desc";
        }
        String query = "SELECT cagendas.fecha, cagendas.hora, centros.nombre_centro,centros.ctro_direcc, servicios.servicio, cagendas.cod_estado, fpersona.nombre_corto FROM cagendas,  centros, servicios, gfh, fpersona WHERE " + where + " " + "ORDER BY " + orderby;
        return query;
    }

}
