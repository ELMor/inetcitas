// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   Cliente.java

package com.ssi.citas.cliente;


public class Cliente
{

    String codigo;
    String tipodocumento;
    String nrodocumento;
    String apellido1;
    String apellido2;
    String nombre;

    public Cliente()
    {
        codigo = "";
        tipodocumento = "";
        nrodocumento = "";
        apellido1 = "";
        apellido2 = "";
        nombre = "";
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String vcodigo)
    {
        codigo = vcodigo;
    }

    public String getTipoDocumento()
    {
        return tipodocumento;
    }

    public void setTipoDocumento(String ptipo)
    {
        tipodocumento = ptipo;
    }

    public String getNroDocumento()
    {
        return nrodocumento;
    }

    public void setNroDocumento(String vnrodocumento)
    {
        nrodocumento = vnrodocumento;
    }

    public String getApellido1()
    {
        return apellido1;
    }

    public void setApellido1(String vapellido1)
    {
        apellido1 = vapellido1;
    }

    public String getApellido2()
    {
        return apellido2;
    }

    public void setApellido2(String vapellido2)
    {
        apellido2 = vapellido2;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String vnombre)
    {
        nombre = vnombre;
    }
}
